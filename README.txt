this mod adds autoclimb to minetest.

this mod is licensed under the LGPL 2.1.

mod version 0.1.

download the mod from contentdb and put/install the mod in the mod folder or download the mod using minetest integraded content downlaoder in the game.

dependencies = default.

report bugs/help in element.io (@mrdraxs:matrix.org).